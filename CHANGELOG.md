# Changelog

## Unreleased

- fix: Make meson actually install things (thanks @craftyguy)
- feat!: Make en-US the default layout
- fix: Use correct pointer type in memset to avoid segfault
- feat: Set correct version in meson.build
- feat: Add postmarketOS theme (thanks @dylanvanassche)
- feat: Handle physical return key (#19)
- fix: Avoid TTY cursor blinking (#26)
- fix: Specify `check: true` when running external commands from meson (#23)
- feat!: Hide partition name (#27)
- fix: Prevent scrolling when keyboard hides (#21)
- feat!: Do not show last typed character when typing (#25)
- feat: Update lvgl & lv_drivers to git master (2022-02-21)

## 0.1.0 (2021-11-15)

Initial release
